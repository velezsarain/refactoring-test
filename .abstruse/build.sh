#!/usr/bin/env bash

main()
{
    for D in `find src/rhoban/model_apps -type d | tail -n +2`; do
        APP=`basename ${D}`
        catkin build rhoban_model_$APP
    done
}

main
