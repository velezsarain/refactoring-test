#!/usr/bin/env cmake -P

execute_process(
  COMMAND ${TEST_TARGET}
  RESULT_VARIABLE TEST_STATUS
  OUTPUT_VARIABLE TEST_OUTPUT
  ERROR_VARIABLE TEST_ERROR
  OUTPUT_FILE ${TEST_TARGET}.out
)

if(TEST_STATUS EQUAL 0)
  if(EXISTS ${TEST_TARGET}.passed)
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E compare_files ${TEST_TARGET}.out ${TEST_TARGET}.passed
      RESULT_VARIABLE TEST_STATUS
      OUTPUT_VARIABLE TEST_OUTPUT
      ERROR_VARIABLE TEST_ERROR
    )

    if(NOT TEST_STATUS EQUAL 0)
      message(FATAL_ERROR "Command \"${CMAKE_COMMAND} -E compare_files ${TEST_OUTPUT_FILE} ${TEST_NAME}.passed\" failed with output:\n${TEST_OUTPUT}\n${TEST_ERROR}")
    endif()
  endif()
else()
  message(FATAL_ERROR "Command \"${TEST_COMMAND}\" failed with output:\n${TEST_OUTPUT}\n${TEST_ERROR}")
endif()
