cmake_minimum_required(VERSION 2.8)
project(rhoban_model)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

find_package(catkin REQUIRED COMPONENTS
  eigen
  lwpr
  libgp
  libcmaes
  rbdl_urdfreader
  RhIOServer
  RhIOClient
  RhAL
)

find_package(SFML REQUIRED COMPONENTS graphics window system)
find_package(Curses REQUIRED)
find_package(OpenGL REQUIRED)

catkin_package(
  INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR} ${SFML_INCLUDE_DIR}
  LIBRARIES model ${SFML_LIBRARIES}
  CATKIN_DEPENDS eigen lwpr libgp libcmaes rbdl_urdfreader RhIOServer RhIOClient RhAL
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${SFML_INCLUDE_DIR}
  ${CURSES_INCLUDE_DIR}
  ${OPENGL_INCLUDE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
)

add_definitions(
  -std=c++11
  -Wall
  -fopenmp
  -DBUILD_RHAL_RHIO_BINDING
  -DLEPH_VIEWER_ENABLED
)

set(SOURCES
  CartWalk/CartWalk.cpp
  CartWalk/linear_algebra.cpp
  CartWalk/math_basics.cpp
  CartWalk/math_log.cpp
  CartWalk/SigmabanLeg.cpp
  CartWalk/CartWalkProxy.cpp
  CartWalk/CartWalkGradient.cpp
  Concepts/HumanoidModelConcept.cpp
  Concepts/HumanoidSensorsModelConcept.cpp
  Concepts/FootStepDifferentiatorConcept.cpp
  Concepts/FootStepIntegratorConcept.cpp
  Concepts/FallDetectorConcept.cpp
  LinearRegression/SimpleLinearRegression.cpp
  LinearRegression/MedianLinearRegression.cpp
  Gradient/FiniteDifferenceGradient.cpp
  Utils/GeometricMedian.cpp
  Utils/Combination.cpp
  Utils/CircularBuffer.cpp
  Utils/Chrono.cpp
  Utils/Scheduling.cpp
  Utils/Differentiation.cpp
  Utils/NewtonBinomial.cpp
  Utils/LWPRUtils.cpp
  Utils/RandomWalk.cpp
  Utils/RandomVelocitySpline.cpp
  Utils/IterativeLearningControl.cpp
  Utils/LWPRInputsOptimization.cpp
  Utils/EncoderFilter.cpp
  Utils/ComputeModelData.cpp
  Utils/FileEigen.cpp
  Utils/FileModelParameters.cpp
  Utils/GaussianDistribution.cpp
  Ncurses/InterfaceCLI.cpp
  Model/Model.cpp
  Model/HumanoidModel.cpp
  Model/HumanoidFloatingModel.cpp
  Model/HumanoidFixedModel.cpp
  Model/HumanoidFixedPressureModel.cpp
  Model/InverseKinematics.cpp
  Model/RBDLRootUpdate.cpp
  Model/RBDLClosedLoop.cpp
  Model/RBDLContactLCP.cpp
  Model/NullSpace.cpp
  Model/ForwardSimulation.cpp
  Model/HumanoidSimulation.cpp
  Model/JointModel.cpp
  Odometry/Odometry.cpp
  Odometry/OdometryDisplacementModel.cpp
  Odometry/OdometryNoiseModel.cpp
  Odometry/OdometrySequence.cpp
  StaticWalk/StaticWalk.cpp
  Spline/Polynom.cpp
  Spline/Spline.cpp
  Spline/SmoothSpline.cpp
  Spline/LinearSpline.cpp
  Spline/CubicSpline.cpp
  Spline/FittedSpline.cpp
  Spline/PolyFit.cpp
  TrajectoryGeneration/TrajectoryGeneration.cpp
  TrajectoryGeneration/TrajectoryUtils.cpp
  TrajectoryGeneration/TrajectoryDisplay.cpp
  LegIK/LegIK.cpp
  IKWalk/IKWalk.cpp
  QuinticWalk/Footstep.cpp
  QuinticWalk/QuinticExperiment.cpp
  QuinticWalk/QuinticWalk.cpp
  TimeSeries/SeriesUtils.cpp
  DMP/DMP.cpp
  DMP/DMPSpline.cpp
  LCPMobyDrake/LCPSolver.cpp
  TrajectoryDefinition/CommonTrajs.cpp
  TrajectoryDefinition/TrajKickSingle.cpp
  TrajectoryDefinition/TrajKickSingleContact.cpp
  TrajectoryDefinition/TrajKickDouble.cpp
  TrajectoryDefinition/TrajStaticPose.cpp
  TrajectoryDefinition/TrajLegLift.cpp
  TrajectoryDefinition/TrajWalk.cpp
  Viewer/ModelViewer.cpp
  Viewer/ModelDraw.cpp
)

set(LIBRARIES
  ${catkin_LIBRARIES}
  ${SFML_LIBRARIES}
  ${CURSES_LIBRARIES}
  ${OPENGL_LIBRARIES}
)

add_library(model SHARED ${SOURCES})
target_link_libraries(model ${LIBRARIES})
