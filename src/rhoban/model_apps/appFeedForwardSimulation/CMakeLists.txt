cmake_minimum_required(VERSION 2.8)
project(rhoban_model_appFeedForwardSimulation)

find_package(catkin REQUIRED COMPONENTS
  rhoban_model
  eigen
  libcmaes
)

catkin_package(
  INCLUDE_DIRS .
  CATKIN_DEPENDS eigen libcmaes rhoban_model
)

include_directories(${catkin_INCLUDE_DIRS})
add_executable(appFeedForwardSimulation appFeedForwardSimulation.cpp)
target_link_libraries(appFeedForwardSimulation ${catkin_LIBRARIES})
