cmake_minimum_required(VERSION 2.8)
project(rhoban_model_appModelParametersViewer)

find_package(catkin REQUIRED COMPONENTS
  rhoban_model
  eigen
)

catkin_package(
  INCLUDE_DIRS .
  CATKIN_DEPENDS eigen rhoban_model
)

include_directories(${catkin_INCLUDE_DIRS})
add_executable(appModelParametersViewer appModelParametersViewer.cpp)
target_link_libraries(appModelParametersViewer ${catkin_LIBRARIES})
