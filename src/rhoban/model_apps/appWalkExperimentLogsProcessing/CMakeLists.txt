cmake_minimum_required(VERSION 2.8)
project(rhoban_model_appWalkExperimentLogsProcessing)

find_package(catkin REQUIRED COMPONENTS
  rhoban_model
)

catkin_package(
  INCLUDE_DIRS .
  CATKIN_DEPENDS rhoban_model
)

include_directories(${catkin_INCLUDE_DIRS})
add_executable(appWalkExperimentLogsProcessing appWalkExperimentLogsProcessing.cpp)
target_link_libraries(appWalkExperimentLogsProcessing ${catkin_LIBRARIES})
