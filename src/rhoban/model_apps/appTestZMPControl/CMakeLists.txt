cmake_minimum_required(VERSION 2.8)
project(rhoban_model_appTestZMPControl)

find_package(catkin REQUIRED COMPONENTS
  eigen
  rhoban_model
)

catkin_package(
  INCLUDE_DIRS .
  CATKIN_DEPENDS eigen rhoban_model
)

include_directories(${catkin_INCLUDE_DIRS})
add_executable(appTestZMPControl appTestZMPControl.cpp)
target_link_libraries(appTestZMPControl ${catkin_LIBRARIES})
